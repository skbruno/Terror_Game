using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;
using UnityEngine.Events;



public class Timer2 : MonoBehaviour
{
    public static Timer2 instance;

    [Header("Timer")]
    public float current_Timer = 0f;
    public float Starting_Timer = 25f;

    public Text countdown_text;

    public Volume low_time;
    private ColorAdjustments coloradjust;
    private Vignette vignette;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        
        current_Timer = Starting_Timer;

        low_time.profile.TryGet(out coloradjust);
        low_time.profile.TryGet(out vignette);
    }

    // Update is called once per frame
    void Update()
    {
        current_Timer -= 1 * Time.deltaTime;
        countdown_text.text = current_Timer.ToString("00:00");

        
        if(current_Timer <= 22f)
        {
            coloradjust.saturation.value = -10f;
            vignette.intensity.value =  Mathf.Lerp(vignette.intensity.value, 0.1f, Time.deltaTime);
        }
        
        if(current_Timer <= 20f)
        {
            coloradjust.saturation.value = -20f;
            vignette.intensity.value =  Mathf.Lerp(vignette.intensity.value, 0.2f, Time.deltaTime);
        }
        if(current_Timer <= 15f)
        {
            coloradjust.saturation.value = -40f;
            vignette.intensity.value =  Mathf.Lerp(vignette.intensity.value, 0.6f, Time.deltaTime);
        }
        if (current_Timer <= 0f)
        {
            Morte_tempo();
        }

        if(current_Timer > 30f)
        {
            coloradjust.saturation.value = 0f;
            vignette.intensity.value =  Mathf.Lerp(vignette.intensity.value, 0f, Time.deltaTime);
        }


    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Rem")
        {
            Debug.Log("a");
            //current_Timer = current_Timer + 20f;
        }

    }

    public void Aumentar_tempo()
    {
        Debug.Log("b");
        current_Timer = current_Timer + 20f;
    }

    void Morte_tempo ()
    {
        StartCoroutine(morte());

    }


    IEnumerator morte()
    {
        UI_Menager.instance.Morte_UI.SetActive(true);
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene(4);

    }


    

}
