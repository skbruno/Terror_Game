using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogicaBrilho : MonoBehaviour
{
    public Slider slider;
    public float sliderValue;
    public Image PainelBrilho;

    // Start is called before the first frame update
    void Start()
    {
        slider.value = PlayerPrefs.GetFloat("brilho", 0.5f);

        PainelBrilho.color = new Color(PainelBrilho.color.r, PainelBrilho.color.g, PainelBrilho.color.b, sliderValue);    
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeSlider(float valor)
    {
        sliderValue = valor;
        PlayerPrefs.SetFloat("brilho", sliderValue);
        PainelBrilho.color = new Color(PainelBrilho.color.r, PainelBrilho.color.g, PainelBrilho.color.b, sliderValue);  
    }
}
