using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class Menu_menager : MonoBehaviour
{
    public static Menu_menager instante;
    
    public static bool game_paused = false;
    public static bool nivelDePause0 = true;
    public static bool nivelDePause1 = false;
    public static bool nivelDePause2 = false;

    public GameObject pausemenuUI;
    public GameObject pausemenuConfigUI;

    void Start()
    {
        nivelDePause0 = true;
        nivelDePause1 = false;
        nivelDePause2 = false;
    }

    void Awake()
    {
        instante = this;

    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            /*
            if (game_paused && game_paused_config == false)
            {
                Resume();
            }

            if (game_paused_config == false)
            {
                Pause();
            }
            */
            
            if (nivelDePause0 == true)
            {
                Pause();
                Debug.Log("ESC Apertado (NivelDePause = 0)");
                nivelDePause0 = false;
                nivelDePause1 = true;
                nivelDePause2 = false;
            }

            else if (nivelDePause1 == true)
            {
                    
                Resume();
                Debug.Log("ESC Apertado (NivelDePause = 1)");
                nivelDePause0 = true;
                nivelDePause1 = false;
                nivelDePause2 = false;

            }
            else if (nivelDePause2 == true)
            {
                pausemenuUI.SetActive(true);
                pausemenuConfigUI.SetActive(false);
                Debug.Log("ESC Apertado (NivelDePause = 2)");
                nivelDePause0 = false;
                nivelDePause1 = true;
                nivelDePause2 = false;
            }
    
        }
    }



    public void Loadscene(string lvlname)
    {
        StartCoroutine(tocarsom());
        SceneManager.LoadScene(lvlname);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
    public void DesativarConfig()
    {
            pausemenuUI.SetActive(true);
            pausemenuConfigUI.SetActive(false);
            nivelDePause0 = false;
            nivelDePause1 = true;
            nivelDePause2 = false;
    }

    public void AtivarConfig()
    {
        nivelDePause0 = false;
        nivelDePause1 = false;
        nivelDePause2 = true;
    }
   
    public void Resume()
    {
        Debug.Log("Resume ativado");
        pausemenuUI.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1f;
        game_paused = false;
        
    }

    public void Pause()
    {
        Debug.Log("Pause ativado");
        Cursor.lockState = CursorLockMode.None;
        pausemenuUI.SetActive(true);
        Cursor.visible = true;
        Time.timeScale = 0f;
        game_paused = true;
        
    }


    public void Morte_Pause()
    {
        Debug.Log("Morte Pause ativado");
        Cursor.lockState = CursorLockMode.None;
        UI_Menager.instance.Morte_UI.SetActive(true);
        Cursor.visible = true;
        Time.timeScale = 0f;
        game_paused = true;
        
    }

    IEnumerator tocarsom()
    {
        FindObjectOfType<Audio_menager>().Play("Confirmação");
        yield return new WaitForSeconds(10f);

    }
}
