using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class Respawn : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        StartCoroutine(morte());
    }

    IEnumerator morte()
    {
        UI_Menager.instance.Morte_UI.SetActive(true);
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene(4);

    }
}
