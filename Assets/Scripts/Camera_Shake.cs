using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Shake : MonoBehaviour
{

    public IEnumerator shake (float duration, float magnitude)
    {
        Vector3 originalPos = transform.localPosition;

        float elapsed = 0.0f;
        Debug.Log("começa2");
        while (elapsed < duration)
        {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-.15f, 1f) * magnitude;

            transform.localPosition = new Vector3 (x, y, originalPos.z);

            yield return null;

            transform.localPosition = originalPos;
        }
    }
}
