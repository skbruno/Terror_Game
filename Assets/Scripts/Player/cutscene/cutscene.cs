using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cutscene : MonoBehaviour
{
    public static cutscene instance;

    [Header("CUTSECENE VERDE")]
    public GameObject player;
    public GameObject cutscene_cam_verde;

    [Header("CUTSECENE VERMELHO")]

    public GameObject cutscene_cam_vermelho;

    [Header("CUTSECENE AZUL")]

    public GameObject cutscene_cam_azul;
    public void Start() 
    {
        instance = this;    
    }
    public void iniciar_cut_verde()
    {
        cutscene_cam_verde.SetActive(true);
        player.SetActive(false);
        StartCoroutine(Finishi_cut_verde());
    }

    IEnumerator Finishi_cut_verde()
    {
        yield return new WaitForSeconds(14);
        player.SetActive(true);
        cutscene_cam_verde.SetActive(false);
    }

    public void iniciar_cut_vermelho()
    {
        Debug.Log("vermelho");
        cutscene_cam_vermelho.SetActive(true);
        player.SetActive(false);
        StartCoroutine(Finishi_cut_vermelho());
    }

    IEnumerator Finishi_cut_vermelho()
    {
        yield return new WaitForSeconds(7);
        player.SetActive(true);
        cutscene_cam_vermelho.SetActive(false);
    }

    public void iniciar_cut_azul()
    {
         Debug.Log("azul");
        cutscene_cam_azul.SetActive(true);
        player.SetActive(false);
        StartCoroutine(Finishi_cut_azul());
    }

    IEnumerator Finishi_cut_azul()
    {
        yield return new WaitForSeconds(8);
        player.SetActive(true);
        cutscene_cam_azul.SetActive(false);
    }
}
