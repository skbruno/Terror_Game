using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ControladorInventario : MonoBehaviour
{
    public bool inventarioOpen;
    public GameObject inventarioHUD;
    public GameObject RemSlot, NescauSlot;
    public Player_Status status;




    public UnityEvent Onview;
    public UnityEvent On_finishi_viwer;
    public UnityEvent Onview_control;
    public UnityEvent On_finishi_contrl;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("i"))
        {
            inventarioOpen = !inventarioOpen;
        }

        if(inventarioOpen == true)
        {
            Cursor.lockState = CursorLockMode.None;
            Player_Interect.instance.COI_INV();
        }
        else if (inventarioOpen == false)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Player_Interect.instance.OUT_INV();
        }
        
        inventarioHUD.SetActive(inventarioOpen);
        Cursor.visible = inventarioOpen;

        ChecarItens();
    }

    void ChecarItens()
    {
        if(status.qtdeDeRem > 0)
        {
            RemSlot.SetActive(true);
        }
        else
        {
            RemSlot.SetActive(false);
        }
    }

    public void UsarItemRem()
    {
        if(status.qtdeDeRem > 0)
        {
            Timer2.instance.Aumentar_tempo();
            status.fomeDeRem += 25;
            status.qtdeDeRem -= 1; 
        }
    }
}
