using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Item", menuName = "Inventario/Criar", order = 1)]

public class InventarioController : ScriptableObject
{
    public string nomeDoItem;
    public Texture2D icone;
    public int quantidade;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
