using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DIsplayItem : MonoBehaviour
{
    public InventarioController item;
    public Text nomeDoItem;
    public Text qtdeDoItemText;
    public RawImage iconeDoItem;

    // Start is called before the first frame update
    void Start()
    {
        qtdeDoItemText.text = "" + item.quantidade;
        iconeDoItem.texture = item.icone;
        nomeDoItem.text = "" + item.nomeDoItem;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
